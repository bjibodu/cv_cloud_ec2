output "bastion_public" {
  value = "${aws_eip.bastion_public.public_ip}"
}
output "pnode_private" {
  value = "${aws_instance.pnode1.private_ip}"
}

output "vde_private" {
  value = "${aws_instance.vde.private_ip}"
}

output "ppml_private" {
  value = "${aws_instance.ppml.private_ip}"
}

output "ppm_private" {
  value = "${aws_instance.ppm.private_ip}"
}

# output "rpndash_private" {
#   value = "${aws_instance.rpndash.private_ip}"
# }
