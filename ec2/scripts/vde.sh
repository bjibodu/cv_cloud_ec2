#!/bin/bash
### pnode installation
sudo sed -i /etc/hosts -e "s/^127.0.0.1 localhost$/127.0.0.1 localhost $(hostname)/"
sudo apt-get update -y > /tmp/update.logs
sudo apt-get install -y  --allow-unauthenticated nfs-common  mysql-server	 curl zlib1g-dev libgomp1

sudo mkdir /efs && sudo chown ubuntu. /efs && sudo mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport 10.22.204.162:/ /efs

#Secure shared memory
echo "tmpfs     /dev/shm     tmpfs     defaults,noexec,nosuid     0     0" >> /etc/fstab

<!-- sudo echo "order bind,hosts" > /etc/host.conf
sudo echo "nospoof on" >> /etc/host.conf




# function users {
	#Remove passwordless sudo once machine has been built
	sudo sed -i '/NOPASSWD:ALL/d' /etc/sudoers

	# #Remove deployment key once built
	# rm /home/cv/.ssh/authorized_keys

	#Create remoteUsers group where membership is required in order to SSH
	groupadd remoteUsers

	sudo	useradd -d /home/cpt -G sudo,admin,adm,cdrom,dip,plugdev -s /bin/bash -m -p '$6$rounds=656000$KxxutzJHaiy.Eoyy$NBh3UCUE2EULJR0TOsczsh0S982Yck7/Ws4YJmlCVezpu9PXxV2hDFg7d.1iLsxgVD//SJSIqeklFsxzyNzyB.' cpt
	sudo			useradd -d /home/ansible -G remoteUsers -s /bin/bash -m -p '$6$rounds=656000$WH7g/OPceKFa8U0u$vSJya7/VzcdK2FIE5mq7xkKpEbA8z.Gjz112WfFIADWozF7C/PtbdQKUvsLul8jQp7lmvzCDMzWAFZcHT6yiu1' ansible
	sudo			useradd -d /home/cv -G remoteUsers -s /bin/bash -m -p '$6$rounds=656000$WH7g/OPceKFa8U0u$vSJya7/VzcdK2FIE5mq7xkKpEbA8z.Gjz112WfFIADWozF7C/PtbdQKUvsLul8jQp7lmvzCDMzWAFZcHT6yiu1' cv

	sudo			useradd -d /home/odavydoff -G sudo,admin,adm,cdrom,dip,plugdev,remoteUsers -s /bin/bash -m -p '$6$rounds=656000$FO5cGXc8S6UPG8Vt$vDaBsxix0PIHREsq5V/0m9EpRPzEZDwcPN9Q6fosqOwqAa3.7kVRUcOp5iozTONBe.gewOWxkDJ.LFhVOxKCI0' odavydoff
	sudo			gpasswd -d cv sudo
	# fi

	# #Extra users for ansible
	# if [ $1 == "a" ] ; then
	sudo			useradd -d /home/snapshots -G remoteUsers -s /bin/bash -m -p '$6$rounds=656000$YWyTa8zOB59rHPV3$rs8lv7XdDoF49f5Ug/aUGG1PODC5TkeA5eItsQfnnuPfRQuF30rXCt4/aMr5eIElT3uSKo0Bo604Qggf/91CX0' snapshots
	sudo			useradd -d /home/databases -G remoteUsers -s /bin/bash -m -p '$6$rounds=656000$zCO0gxQWGniXpnfv$5J6rrcm.JG3MHGMiN1is2ET8X0Mxaty/Y0hQU3NcNnmEY/adsxFoiLGrt0cLbj6S8i5Q4IcsGeUzcm0vBfDeq1' databases
	sudo			useradd -d /home/ansible -s /bin/bash -m -p '$6$rounds=656000$WH7g/OPceKFa8U0u$vSJya7/VzcdK2FIE5mq7xkKpEbA8z.Gjz112WfFIADWozF7C/PtbdQKUvsLul8jQp7lmvzCDMzWAFZcHT6yiu1' ansible
	# fi


	sudo unlink /etc/motd
	sudo sh -c "echo '############################################################################' > /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '#  WARNING:  Unauthorized access to this system is forbidden and will be   #' >> /etc/motdStatic"
	sudo sh -c "echo '# prosecuted by law. By accessing this system, you agree that your actions #' >> /etc/motdStatic"
	sudo sh -c "echo '#          may be monitored if unauthorized usage is suspected.            #' >> /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '############################################################################' >> /etc/motdStatic"
	sudo sh -c "echo '' >> /etc/motdStatic"
	sudo sh -c "echo '' >> /etc/motdStatic"
	sudo ln -s /etc/motdStatic /etc/motd

### libs

	echo "/jss_cv/lib" > /etc/ld.so.conf.d/cv.conf
	echo "/usr/local/lib" >> /etc/ld.so.conf.d/cv.conf

	cd ~
	mkdir /jss_cv
	# if [ $1 == "p" ] ; then
	# 	if [ -n "$cvRepo" ] ; then
		# 	apt-get install -y --allow-unauthenticated jss-cv daemon macrotracker
		# else
	sudo		dpkg -i /efs/cv14/jss-cv*
	sudo	  dpkg -i /efs/cv14/daemon_*
  sudo 		dpkg  -i  /efs/cv14/macrotracker*
  sudo    dpkg -i /efs/cv14/visual_data_explorer* #### vde node
		# fi
