
#!/bin/bash -v

# sudo sed -i /etc/hosts -e "s/^127.0.0.1 localhost$/127.0.0.1 localhost $(hostname)/"
# slepp 10
# <!-- cd /home/cv/Ubuntu$ubuntu/
# dpkg -i libdpkg-perl_*
# dpkg -i binutils_*
# dpkg -i make_*
# dpkg -i dpkg-dev_*
# # cd /home/cv/
# dpkg-scanpackages Ubuntu$ubuntu | gzip > Ubuntu$ubuntu/Packages.gz
# # echo "deb file:///home/cv Ubuntu$ubuntu/" > /etc/apt/sources.list -->
# sudo echo "nameserver 8.8.8.8" >> /etc/resolv.conf
# sudo sed -i /etc/hosts -e "s/^127.0.0.1 localhost$/127.0.0.1 localhost $(hostname)/"
 apt-get update -y > /tmp/update.logs

sleep 10
#Remove conflicting package
sudo apt-get --purge remove -y libsdl1.2-dev

sudo apt-get install mysql-client -y
touch /tmp/bolaji.txt
#Secure shared memory
echo "tmpfs     /dev/shm     tmpfs     defaults,noexec,nosuid     0     0" >> /etc/fstab

<!-- sudo echo "order bind,hosts" > /etc/host.conf
sudo echo "nospoof on" >> /etc/host.conf

 #Secure network settings
sudo sed -i "s/#net.ipv4.conf.default.rp_filter=1/net.ipv4.conf.default.rp_filter=1/" /etc/sysctl.conf
sudo sed -i "s/#net.ipv4.conf.all.rp_filter=1/net.ipv4.conf.all.rp_filter=1/" /etc/sysctl.conf
sudo sed -i "s/#net.ipv4.conf.all.accept_source_route = 0/net.ipv4.conf.all.accept_source_route = 0/" /etc/sysctl.conf
sudo sed -i "s/#net.ipv6.conf.all.accept_source_route = 0/net.ipv6.conf.all.accept_source_route = 0/" /etc/sysctl.conf
sudo sed -i "s/#net.ipv4.conf.all.accept_redirects = 0/net.ipv4.conf.all.accept_redirects = 0\nnet.ipv4.conf.default.accept_redirects = 0/" /etc/sysctl.conf
sudo sed -i "s/# net.ipv4.conf.all.secure_redirects = 1/net.ipv4.conf.all.secure_redirects = 0\nnet.ipv4.conf.default.secure_redirects = 0/" /etc/sysctl.conf
sudo sed -i "s/#net.ipv4.conf.all.log_martians = 1/net.ipv4.conf.all.log_martians = 1\nnet.ipv4.conf.default.log_martians = 1/" /etc/sysctl.conf
sudo echo -e "\n# Block SYN attacks" >> /etc/sysctl.conf
sudo echo "net.ipv4.tcp_syncookies = 1" >> /etc/sysctl.conf
sudo echo "net.ipv4.tcp_max_syn_backlog = 2048" >> /etc/sysctl.conf
sudo echo "net.ipv4.tcp_synack_retries = 2" >> /etc/sysctl.conf
sudo echo -e "net.ipv4.tcp_syn_retries = 5\n" >> /etc/sysctl.conf
sudo echo "# Ignore send redirects" >> /etc/sysctl.conf
sudo  echo "net.ipv4.conf.all.send_redirects = 0" >> /etc/sysctl.conf
sudo echo -e "\nnet.ipv4.conf.default.send_redirects = 0\n" >> /etc/sysctl.conf
sudo echo "net.ipv4.conf.default.accept_source_route = 0" >> /etc/sysctl.conf
sudo echo -e "net.ipv6.conf.default.accept_source_route = 0\n" >> /etc/sysctl.conf
sudo echo "net.ipv4.icmp_ignore_bogus_error_responses = 1" >> /etc/sysctl.conf
sudo echo -e "net.ipv6.conf.all.accept_ra = 0\nnet.ipv6.conf.default.accept_ra = 0" >> /etc/sysctl.conf
sudo echo -e "net.ipv6.conf.all.accept_redirect = 0\nnet.ipv6.conf.default.accept_redirect = 0" >> /etc/sysctl.conf
sudo sysctl -p

#Disable IPv6
sudo sed -i 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="ipv6.disable=1"/' /boot/grub/grub.cfg -->

#----------------------CIS security fixes-----------------------------
#Disable unused filesystems
echo 'install cramfs /bin/true
install freevxfs /bin/true
install jffs2 /bin/true
install hfs /bin/true
install hfsplus /bin/true
install squashfs /bin/true
install udf /bin/true
install vfat /bin/true' > /etc/modprobe.d/CIS.conf

#Set nodev on /home partition
sed "s/home           ext4    defaults/home           ext4    nodev/g" /etc/fstab

#Set noexec on /run/shm partition
echo "none            /run/shm                  tmpfs           nosuid,nodev,noexec                          0 0" >> /etc/fstab

#Set bootloader conf to root 600 permissions
chmod 600 /boot/grub/grub.cfg

#Restrict core dumps
echo '* hard core 0' >> /etc/security/limits.conf
echo 'fs.suid_dumpable = 0' >> /etc/sysctl.conf

#restrict core dumps and disable IPv6 on boot
echo "sysctl -w fs.suid_dumpable=0" > /etc/rc.local
echo "sysctl -w net.ipv6.conf.all.accept_redirects=0" >> /etc/rc.local
echo "sysctl -w net.ipv6.conf.default.accept_redirects=0" >> /etc/rc.local
echo "sysctl -w net.ipv6.route.flush=1" >> /etc/rc.local

#Configure local and remote login warning banners
rm -f /etc/issue
rm -f /etc/issue.net
ln -s /etc/motdStatic /etc/issue
ln -s /etc/motdStatic /etc/issue.net

#Remove all X window system applications
apt-get --purge remove -y xserver-xorg-core xserver-xorg-core-no-multiarch

#Ensure NFS and RPC are not enabled
sed -i "s/start on start-rpcbind/#start on start-rpcbind/g" /etc/init/rpcbind.conf
update-rc.d nfs-kernel-server disable

#Ensure telnet client is not installed
apt-get --purge remove -y telnet

#Set correct permissions on cron files
chmod 600 /etc/crontab
chmod 700 /etc/cron.d /etc/cron.daily /etc/cron.hourly /etc/cron.monthly /etc/cron.weekly

#Secure SSH config and settings
chmod 600 /etc/ssh/sshd_config
<!-- sed -i "s/X11Forwarding yes/X11Forwarding no/g" /etc/ssh/sshd_config -->
sed -i "s/PermitRootLogin without-password/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i "s/LoginGraceTime 120/LoginGraceTime 30/g" /etc/ssh/sshd_config
echo -e "\n#Set number of attempted key logins\nMaxAuthTries 4" >> /etc/ssh/sshd_config
echo -e "\n#Disable user environment\nPermitUserEnvironment no" >> /etc/ssh/sshd_config
<!-- echo -e "\n#Specify allowable hashing algorithms\nMACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com" >> /etc/ssh/sshd_config -->
echo -e "\n#Time client out after five minutes of inactivity and don't send any timeout messages\nClientAliveInterval 300" >> /etc/ssh/sshd_config
echo -e "ClientAliveCountMax 0" >> /etc/ssh/sshd_config
echo -e "\n#Set banner message\nBanner /etc/issue.net" >> /etc/ssh/sshd_config


<!-- #Secure grub boot if selected
if [ $bootPass == "yes" ] ; then
  echo 'set superusers="booter"' >> /etc/grub.d/40_custom
  echo 'password_pbkdf2 booter grub.pbkdf2.sha512.10000.4EB016B1AFA3EB514BD0A6BFDB92014FD09EA2B6DD660D5139C34456E188D9C52BF47B7DCA9C96396C424787F1F643D1A24C72B6AF51DF9A76C9847A614B7186.49465EA5E429E8242E493097ADC179E92B9AE50D64CAB97CD9B44A72227CABC0C14053BA70DF18038D3E697FB8731FD63231A06C7AFBEFCB531A4886BF645732' >> /etc/grub.d/40_custom
  update-grub
fi

#Configure password complexity checks
sed -i "s/pam_pwquality.so retry=3/pam_pwquality.so retry=3 minlen=10 dcredit=-1 lcredit=-1 ocredit=-1 ucredit=-1/g" /etc/pam.d/common-password
sed -i 's:here are the per-package modules (the "Primary" block):here are the per-package modules (the "Primary" block)\nauth    required                        pam_tally2.so  file=/var/log/tallylog deny=3 even_deny_root unlock_time=1200\naccount required                        pam_tally2.so:g' /etc/pam.d/common-auth
sed -i 's/try_first_pass sha512/try_first_pass sha512 remember=5/g' /etc/pam.d/common-auth -->



function apacheConf {
	packageCheck "apache2" "rec" 5
	packageCheck "libapache2-modsecurity" "rec" 5
	packageCheck "libapache2-mod-evasive" "rec" 5

	#Still doesn't work for Ubuntu 14 because all the configuration files have been messed around with
	#Change apache ports from 80 to 8001 and 443 to 9001
	sed -i "s/80/8001/" /etc/apache2/ports.conf
	sed -i "s/80/8001/" /etc/apache2/sites-enabled/000-default
	sed -i "s/443/9001/" /etc/apache2/ports.conf
	sed -i "s/443/9001/" /etc/apache2/sites-available/default-ssl

	#Restrict apache information leakage
	sed -i "s/ServerTokens OS/ServerTokens Prod/" /etc/apache2/conf.d/security
	sed -i "s/ServerSignature On/ServerSignature Off/" /etc/apache2/conf.d/security
	sed -i "s/TraceEnable On/TraceEnable Off/" /etc/apache2/conf.d/security
	echo "FileETag None" >> /etc/apache2/conf.d/security

	#disable SSL v3 support
	sed -i "s/SSLProtocol all -SSLv2/SSLProtocol all -SSLv2 -SSLv3/" /etc/apache2/mods-available/ssl.conf

	ln -s /etc/apache2/mods-available/headers.load /etc/apache2/mods-enabled/headers.load
	mv /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf
	sed -i "s/SecRuleEngine Off/SecRuleEngine On/" /etc/modsecurity/modsecurity.conf
	sed -i "s/SecRequestBodyLimit 16384000/SecRequestBodyLimit 16384000/" /etc/modsecurity/modsecurity.conf
	sed -i "s/SecRequestBodyInMemoryLimit 16384000/SecRequestBodyInMemoryLimit 16384000/" /etc/modsecurity/modsecurity.conf

	<!-- cd /tmp
	if [ "$host" == "l" ] ; then
		cp /home/cv/files/master.zip .
	else
		wget http://$cvRepo:8001/repo/files/master.zip
	fi
	packageCheck "zip" "rec" 5
	unzip master.zip
	cp -r owasp-modsecurity-crs-master/* /etc/modsecurity/
	mv /etc/modsecurity/modsecurity_crs_10_setup.conf.example /etc/modsecurity/modsecurity_crs_10_setup.conf
	ls /etc/modsecurity/base_rules | xargs -I {} ln -s /etc/modsecurity/base_rules/{} /etc/modsecurity/activated_rules/{}
	ls /etc/modsecurity/optional_rules | xargs -I {} ln -s /etc/modsecurity/optional_rules/{} /etc/modsecurity/activated_rules/{}

	mkdir /var/log/mod_evasive
	chown www-data:www-data /var/log/mod_evasive
	echo "DOSSystemCommand On" >> /etc/apache2/mods-available/evasive.conf
	ln -s /etc/apache2/mods-available/evasive.conf /etc/apache2/mods-enabled/evasive.conf
} -->
#------------------------------------------------------------
<!-- function phpConf {
	for i in libapache2-mod-php5 php5-cli php5-common php5-gd php5-mcrypt php5-mysql ; do
		packageCheck "$i"  "rec" 5
	done
	sed -i '/^disable_functions/ s/$/exec,system,shell_exec,passthru,/' /etc/php5/apache2/php.ini
	sed -i 's/register_globals = On/register_globals = Off/' /etc/php5/apache2/php.ini
	sed -i 's/expose_php = On/expose_php = Off/' /etc/php5/apache2/php.ini
	sed -i 's/display_errors = On/display_errors = Off/' /etc/php5/apache2/php.ini
	sed -i 's/track_errors = On/track_errors = Off/' /etc/php5/apache2/php.ini
	sed -i 's/html_errors = On/html_errors = Off/' /etc/php5/apache2/php.ini
	sed -i 's/magic_quotes_gpc = On/magic_quotes_gpc = Off/' /etc/php5/apache2/php.ini
}

function libs {
	#Make sure binaries are pointing to the right place
	echo "/jss_cv/lib" > /etc/ld.so.conf.d/cv.conf
	echo "/usr/local/lib" >> /etc/ld.so.conf.d/cv.conf

	#Configure libraries and binaries
	cd ~
	mkdir /jss_cv
	if [ $1 == "p" ] ; then
		if [ -n "$cvRepo" ] ; then
			apt-get install -y --allow-unauthenticated jss-cv daemon macrotracker
		else
			dpkg -i /home/cv/jss_cv*
			dpkg -i /home/cv/daemon_global*
			dpkg -i /home/cv/macrotracker*
		fi
		echo -e "{\n\t\"database\":\n\t{\n\t\t\"host\" : \"127.0.0.1\",\n\t\t\"port\" : 6033,\n\t\t\"schema\" : \"jss_cv\",\n\t\t\"user\" : \"jss_cv\",\t\"password\":\"\",\n\t\t\"timeout\" : 10\n\t}\n}" > /jss_cv/daemon_global/.config
		autoStart

		#Automatically rotate cv logs on pnodes
		su cv -c '(crontab -l 2>/dev/null; echo -e "#Find and delete logs older than a day to prevent /var/log/ from filling up\n00 * * * * find /var/log/cv/ -type f -mtime +30 | xargs rm") | crontab -'
	elif [ $1 == "v" ] ; then
		if [ -n "$cvRepo" ] ; then
			apt-get install -y --allow-unauthenticated jss-cv daemon vde ppm
		else
			dpkg -i /home/cv/jss_cv*
			dpkg -i /home/cv/daemon_global*
			dpkg -i /home/cv/post_processing_module*
			dpkg -i /home/cv/visual_data_explorer*
		fi
		echo -e "{\n\t\"database\":\n\t{\n\t\t\"host\" : \"127.0.0.1\",\n\t\t\"port\" : 6033,\n\t\t\"schema\" : \"jss_cv\",\n\t\t\"user\" : \"jss_cv\",\t\"password\":\"\",\n\t\t\"timeout\" : 10\n\t}\n}" > /jss_cv/daemon_global/.config
		autoStart

		#Automatically rotate cv logs on vde/ppm nodes
		su cv -c '(crontab -l 2>/dev/null; echo -e "#Find and delete logs older than a day to prevent /var/log/ from filling up\n00 * * * * find /var/log/cv/ -type f -mtime +30 | xargs rm") | crontab -'
	elif [ $1 == "t" ] ; then
		if [ -n "$cvRepo" ] ; then
			apt-get install -y --allow-unauthenticated jss-cv daemon macrotracker vde ppm
		else
			dpkg -i /home/cv/jss_cv*
			dpkg -i /home/cv/daemon_global*
			dpkg -i /home/cv/macrotracker*
			dpkg -i /home/cv/post_processing_module*
			dpkg -i /home/cv/visual_data_explorer*
		fi
		echo -e "{\n\t\"database\":\n\t{\n\t\t\"host\" : \"127.0.0.1\",\n\t\t\"port\" : 6033,\n\t\t\"schema\" : \"jss_cv\",\n\t\t\"user\" : \"jss_cv\",\t\"password\":\"\",\n\t\t\"timeout\" : 10\n\t}\n}" > /jss_cv/daemon_global/.config
		autoStart
	elif [ $1 == "a" ] ; then
		mkdir /jss_cv/scripts
	fi

	if [ $ubuntu -eq 16 ] ; then
		sudo systemctl enable rc-local.service
	fi

	mv /home/cv/dispatcher/ /jss_cv/
	ldconfig
	chown -R cv:cv /jss_cv
} -->
#------------------------------------------------------------
<!-- function autoStart {
	#Make the auto start directory required for global daemon and then write to a file contained inside with the information
#	mkdir -p /home/cv/.config/autostart/
#	echo -e "[Desktop Entry]\nType=Application\nExec=/jss_cv/daemon_global/daemon_global /jss_cv/daemon_global/.config\nHidden=false\nNoDisplay=false\nX-GNOME-Autostart-enabled=true\nName[en_GB]=Global Daemon\nName=Global Daemon\nComment[en_GB]=\nComment=" > /home/cv/.config/autostart/daemon_global.desktop
#	chown -R cv:cv /home/cv/.config/ /home/cv/.gnupg/ /home/cv/.viminfo
	if [ "$host" == "p" ] ; then
		echo -e "#Automatically start global daemon on boot\nsleep 10\nsu cv -c '/jss_cv/daemon_global/daemon_global /jss_cv/daemon_global/.config &'\n#Automatically activate crowdvision license\n/root/activate.sh\nexit 0" >> /etc/rc.local
	else
		echo -e "#Automatically start global daemon on boot\nsu cv -c '/jss_cv/daemon_global/daemon_global /jss_cv/daemon_global/.config &'\nexit 0" >> /etc/rc.local
	fi
}
#------------------------------------------------------------ -->
<!-- function license {
	echo '#!/bin/bash

token="$(echo hostname $(ifconfig | grep -i hwaddr | tr -s " " | cut -d " " -f5 | tr -d ":") $(ifconfig | grep -i "inet addr" | tr -s " " | cut -d " " -f3 | grep -v "127.0.0.1" | cut -d ":" -f2 | tr -d ".") | tr -d " " | sha256sum | cut -d " " -f1)"
license="$(mysql -h '$licenseServer' -P 6033 -u licenses cameras -pL1c8Nses! -e "select licenseKey from licenses where token=\"$token\" and enabled=1" | tail -n1)"
touch /tmp/"$license" && echo "$license" > /tmp/"$license"
chown cv:cv /tmp/"$license"' > /root/activate.sh
	chmod 700 /root/activate.sh
} -->

function users {
	#Remove passwordless sudo once machine has been built
	sudo sed -i '/NOPASSWD:ALL/d' /etc/sudoers

	#Remove deployment key once built
	rm /home/cv/.ssh/authorized_keys

	#Create remoteUsers group where membership is required in order to SSH
	groupadd remoteUsers

	#Create initial system users
	if [ $1 != "t" ] ; then
		usermod -p '$6$rounds=656000$3kVQyptw2o2LZUKS$wPR9Ow0zdlFfK.CCjEtnhFK/91pZTI9DWUnkdGe8yFamwE/hAmdfpHAHdgK9lGTgABOXnxc7coipptULrplWf0' root
		usermod -a -G remoteUsers cv
		useradd -d /home/cpt -G sudo,admin,adm,cdrom,dip,plugdev -s /bin/bash -m -p '$6$rounds=656000$KxxutzJHaiy.Eoyy$NBh3UCUE2EULJR0TOsczsh0S982Yck7/Ws4YJmlCVezpu9PXxV2hDFg7d.1iLsxgVD//SJSIqeklFsxzyNzyB.' cpt
		useradd -d /home/ansible -G remoteUsers -s /bin/bash -m -p '$6$rounds=656000$WH7g/OPceKFa8U0u$vSJya7/VzcdK2FIE5mq7xkKpEbA8z.Gjz112WfFIADWozF7C/PtbdQKUvsLul8jQp7lmvzCDMzWAFZcHT6yiu1' ansible
		useradd -d /home/odavydoff -G sudo,admin,adm,cdrom,dip,plugdev,remoteUsers -s /bin/bash -m -p '$6$rounds=656000$FO5cGXc8S6UPG8Vt$vDaBsxix0PIHREsq5V/0m9EpRPzEZDwcPN9Q6fosqOwqAa3.7kVRUcOp5iozTONBe.gewOWxkDJ.LFhVOxKCI0' odavydoff
		useradd -d /home/glantair -G sudo,admin,adm,cdrom,dip,plugdev,remoteUsers -s /bin/bash -m -p '$6$rounds=656000$py2tpkpIdWPmvwaL$5vzSYp04jYVAa.CWTR4M.lQlIsoei0lnv4jvQS6cD/CuamArt9NZC5kwrMWbEZgVRp6bEhV7jtGXxVECivGuG0' glantair
		useradd -d /home/ntrigger -G sudo,admin,adm,cdrom,dip,plugdev,remoteUsers -s /bin/bash -m -p '$6$rounds=656000$BIHvrPy2/xflquOR$vzlR1s1aeB2VZxZipdNJttWGGHfycs2zISVXSmVcKIAmp1TdesUe/lZa/8zXV1mtB8blh5DebNmnoiVBolMcw.' ntrigger
		useradd -d /home/rwebb -G sudo,admin,adm,cdrom,dip,plugdev,remoteUsers -s /bin/bash -m -p '$6$rounds=656000$QWJfJgqx5SLPpAbU$HC6TEAfbT75w/xe.DK8Jr2S2SDpCnWNretvHVzW9uY02DYsG44oeCL4pa87biPg/jHwEjmH70kGkJnJxJ43Mq.' rwebb
		useradd -d /home/pliu -G sudo,admin,adm,cdrom,dip,plugdev,remoteUsers -s /bin/bash -m -p '$6$rounds=656000$expjRwwRT2hKRPP0$fneJ.1fWUYhhjoZxlImbxvNPkTtZ3ErD.HAIfxybdxJzBBTgzXen1zJFaiIYWnDY2VW3VQAUbzFG4CiYPUri..' pliu
		gpasswd -d cv sudo
	fi

	#Extra users for ansible
	if [ $1 == "a" ] ; then
		useradd -d /home/snapshots -G remoteUsers -s /bin/bash -m -p '$6$rounds=656000$YWyTa8zOB59rHPV3$rs8lv7XdDoF49f5Ug/aUGG1PODC5TkeA5eItsQfnnuPfRQuF30rXCt4/aMr5eIElT3uSKo0Bo604Qggf/91CX0' snapshots
		useradd -d /home/databases -G remoteUsers -s /bin/bash -m -p '$6$rounds=656000$zCO0gxQWGniXpnfv$5J6rrcm.JG3MHGMiN1is2ET8X0Mxaty/Y0hQU3NcNnmEY/adsxFoiLGrt0cLbj6S8i5Q4IcsGeUzcm0vBfDeq1' databases
		useradd -d /home/ansible -s /bin/bash -m -p '$6$rounds=656000$WH7g/OPceKFa8U0u$vSJya7/VzcdK2FIE5mq7xkKpEbA8z.Gjz112WfFIADWozF7C/PtbdQKUvsLul8jQp7lmvzCDMzWAFZcHT6yiu1' ansible
	fi

	#Make sure only authorised users are allowed to SSH
	echo -e "\n#Whitelist for authorised users\nAllowGroups remoteUsers" >> /etc/ssh/sshd_config
#	if [ $1 == "a" ] ; then
#Get some other user accounts ot put her for testing purposes
		#useradd -d /home/mdellagana -s /usr/sbin/nologin -m -p '$6$rounds=656000$ei1O.ECVTl2GWr2Y$TDisaduacTPuFZzCBMJ5nPPLDCgwBe91AjxVwv65CLj7cbQl4U5TPDZZfGQ/KMM2zpcB6WyU46pnVQegd3unk0' mdellagana
#	fi
}
#------------------------------------------------------------
function motd {
	sudo unlink /etc/motd
	sudo sh -c "echo '############################################################################' > /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '#  WARNING:  Unauthorized access to this system is forbidden and will be   #' >> /etc/motdStatic"
	sudo sh -c "echo '# prosecuted by law. By accessing this system, you agree that your actions #' >> /etc/motdStatic"
	sudo sh -c "echo '#          may be monitored if unauthorized usage is suspected.            #' >> /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '#                                                                          #' >> /etc/motdStatic"
	sudo sh -c "echo '############################################################################' >> /etc/motdStatic"
	sudo sh -c "echo '' >> /etc/motdStatic"
	sudo sh -c "echo '' >> /etc/motdStatic"
	sudo ln -s /etc/motdStatic /etc/motd
}
#------------------------------------------------------------
function name {
	sudo sh -c "echo '$serverName' > /etc/hostname"
	current=$(hostname)
	sudo sh -c "sed -i 's/$current/$serverName/g' /etc/hosts"
}
#------------------------------------------------------------
<!-- function repo {
	#Fetch and add repository key
#	sudo sh -c "wget -O - http://$cvRepo:8001/repo/ubuntuRep$ubuntu/conf/cv.gpg.key|apt-key add -"
	<!-- sudo sh -c "wget -O - http://$cvRepo:8001/repo/master/ubuntu$ubuntu/system/conf/cv.gpg.key|apt-key add -" -->

	#Determine osCode name
	osCode="$(lsb_release -a | grep Codename | cut -f2)"

#	echo -e "###### Custom Crowdvision apt repositories\ndeb http://$cvRepo:8001/repo/ubuntuRep$ubuntu $osCode main\ndeb http://$cvRepo:8001/repo/cvRep$ubuntu $osCode main" > /etc/apt/sources.list
	echo -e "###### Custom Crowdvision apt repositories\ndeb http://$cvRepo:8001/repo/master/ubuntu$ubuntu/system/ $osCode main\ndeb http://$cvRepo:8001/repo/master/ubuntu$ubuntu/$airportCode/ $osCode main" > /etc/apt/sources.list
	apt-get update
} -->
#------------------------------------------------------------
<!-- function reboot {
	rm -rf /home/cv/dispatcher/ /home/cv/bare_jss_cv.sql /home/cv/ani* /home/cv/*.deb /home/cv/cv/ /home/cv/files/ /home/cv/Ubuntu* /home/cv/mountBarracuda.sh /home/cv/build*
	if [ "$auto" == "yes" ] ; then
		sudo telinit 6
	else
		printf "\nReboot host now to ensure all configuration settings are applied? y/n\n"
		read response
		if [ $response == "y" ] ; then
			sudo telinit 6
		fi
	fi
}
#------------------------------------------------------------
function partition {
	hdd="/dev/sdb"

	#Pass in arguments to fdisk in order to create a new partion on /dev/sdb
	echo "n
	p



	w
	" | fdisk $hdd

	#Format new partition to ext4
	mkfs.ext4 $hdd"1"

	#Create mount folder and then mount partition there
	mkdir /media/mysql
	mount /dev/sdb1 /media/mysql

	#Get uuid for new partition and update /etc/fstab with new partition information in order to be persistent on boot
	uuid=$(blkid | grep sdb1 | cut -d " " -f2)
	echo -e "\n#Partition for /media/mysql\n$uuid /media/mysql           ext4    nosuid,nodev,noexec        0       2" >> /etc/fstab

	#Stop MySQL, move database to /media/mysql partition and update my.cnf to point to new location
	if [ $host != "l" ] ; then
		service mysql stop
		mv /var/lib/mysql/ /media/mysql/db
		chown -R mysql:mysql /media/mysql/db
		ln -s /media/mysql/db/ /var/lib/mysql
		chown -R mysql:mysql /var/lib/mysql
		sed -i "s;datadir		= /var/lib/mysql;datadir		= /media/mysql/db;g" $mysqlConf
		sed -i "s/innodb_buffer_pool_size = 160M/innodb_buffer_pool_size = 2000M/g" $mysqlConf

		#Update apparmor profiles before reloading and resuming MySQL
		sed -i "s;/var/lib/mysql/ r;/media/mysql/db/ r;g" /etc/apparmor.d/usr.sbin.mysqld
		sed -i "s;/var/lib/mysql/\*\* rwk;/media/mysql/db/\*\* rwk;g" /etc/apparmor.d/usr.sbin.mysqld
		service apparmor reload
		service mysql start
	fi
} -->

#------------------------------------------------------------
function buildRepPortal {
	<!-- #Building the reporting portal sits outside the other more standardised types because it contains a lot extra dependencies that have not yet been investigated enough to determine what can and can't be secured or removed. Whilst ugly this build exists as it's own unique build at the moment so operates outside of the others.

	#Prompt for bitbucket information
	printf "\nEnter Bitbucket password for $user\n"
	read -s pw

	#Prompt for initial reporting portal account password
	printf "\nEnter initial password for reporting portal user $rootEmail\n"
	read -s rootPw -->


	##Set the system up
	sudo apt-get update -y
	sudo apt-get upgrade -y
	sudo apt-get install -y --allow-unauthenticated git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev libgdbm-dev libncurses5-dev automake libtool bison libffi-dev apache2


	#Install Ruby
	gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
	curl -L https://get.rvm.io | bash -s stable
	source ~/.rvm/scripts/rvm
	rvm reload
	rvm install 2.1.0
	rvm use 2.1.0 --default
	echo "gem: --no-ri --no-rdoc" > ~/.gemrc


	##Install node.js
	sudo apt-get install -y --allow-unauthenticated nodejs
	gem install bundler


	#Install rails
	gem install rails
	gem install thin
	thin install


	#Install Mongo
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
	echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
	sudo apt-get update -y
	sudo apt-get install -y --allow-unauthenticated mongodb-org=2.6.1 mongodb-org-server=2.6.1 mongodb-org-shell=2.6.1 mongodb-org-mongos=2.6.1 mongodb-org-tools=2.6.1
	echo "mongodb-org hold" | sudo dpkg --set-selections
	echo "mongodb-org-server hold" | sudo dpkg --set-selections
	echo "mongodb-org-shell hold" | sudo dpkg --set-selections
	echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
	echo "mongodb-org-tools hold" | sudo dpkg --set-selections


	#Install redis-server
	sudo apt-get install -y --allow-unauthenticated redis-server


	#Clone repo
	git clone --depth=1 "https://"$user":"$pw"@bitbucket.org/anicrowd/"$repo."git"
	rm -rf $repo/.git
	rm -rf $repo/.DS_Store
	rm -rf $repo/.gitignore
	sudo mkdir /var/www/$repo
	sudo rsync -avr $repo/ /var/www/$repo/
	cd /var/www/$repo/
	bundle install
	rake db:seed --trace
	rake db:setup RAILS_ENV="production"
	RAILS_ENV=production bundle exec rake assets:precompile
	rvmsudo thin install
	rvmsudo thin config -C /etc/thin/rpn.yml -c /var/www/$repo/ -p 3000 -e production


	#Setup root user
	rails runner "Setting.create!({:account_allowed => 1})"
	rails runner "User.create!({:email => \"$rootEmail\", :password => \"$rootPw\", :password_confirmation => \"$rootPw\" })"


	#Setup god and queue
	god -c config/resque.god
	god load config/resque.god
	god load config/resque_scheduler.god
	god restart


	#Start the web portal
	rvmsudo /etc/init.d/thin start
}
#------------------------------------------------------------
function buildRepPortalNew {
	<!-- #Building the reporting portal sits outside the other more standardised types because it contains a lot extra dependencies that have not yet been investigated enough to determine what can and can't be secured or removed. Whilst ugly this build exists as it's own unique build at the moment so operates outside of the others.
	#Not updated yet to incorporate MySQL table level replication and mysql root password must be set or eles ruby will fail to connect

	#Prompt for bitbucket information
	printf "\nEnter Bitbucket password for $user\n"
	read -s pw

	#Prompt for initial reporting portal account password
	printf "\nEnter initial password for reporting portal user $rootEmail\n"
	read -s rootPw

	#Prompt for initial reporting portal account password
	printf "\nEnter root password for MySQL\n"
	read -s mysqlRoot

	#Gather required information for database.yml config
	dbFile="/var/www/html/$repoNew/config/database.yml"
	localDBUser="jss_cv"
	dashDBUser="cv_dash"
	rpnDBUser="cv_rpn"
	DBPort=6033
	rubyVersion="2.2.2"

	printf "\nEnter password for local database connection from $localDBUser:\n"
	read -s localDBPass

	printf "\nEnter password for LAE database connection from $dashDBUser:\n"
	read -s dashDBPass

	printf "\nEnter password for PAE database connection from $rpnDBUser:\n"
	read -s rpnDBPass

	printf "\nEnter IP for LAE server:\n"
	read dashIP

	printf "\nEnter IP for PAE server:\n"
	read rpnIP

	##Set the system up
	sudo apt-get update -y
	sudo apt-get upgrade -y
	sudo apt-get install -y --allow-unauthenticated git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev libgdbm-dev libncurses5-dev automake libtool bison libffi-dev apache2 libgmp-dev
	sudo sh -c "echo 'mysql-server mysql-server/root_password password $mysqlRoot' | debconf-set-selections"
	sudo sh -c "echo 'mysql-server mysql-server/root_password_again password $mysqlRoot' | debconf-set-selections"
	sudo apt-get install -y --allow-unauthenticated mysql-server libmysqlclient-dev -->

<!--
	#Install Ruby
	curl -sSL https://rvm.io/mpapis.asc | gpg --import -

	curl -L https://get.rvm.io | bash -s stable
	source ~/.rvm/scripts/rvm
	rvm reload
	rvm install $rubyVersion
	rvm use $rubyVersion --default
	echo "gem: --no-ri --no-rdoc" > ~/.gemrc

	#Might need but not sure if required every time
	sudo ln -s ~/.rvm/rubies/ruby-$rubyVersion/bin/ruby /usr/bin/ruby


	##Install node.js
	sudo apt-get install -y --allow-unauthenticated nodejs
	gem install bundler


	#Install rails
	gem install rails


	#Install redis-server
	sudo apt-get install -y --allow-unauthenticated redis-server


	#Clone repo
	git clone --depth=1 "https://"$user":"$pw"@bitbucket.org/anicrowd/"$repoNew."git"
	rm -rf $repoNew/.git
	rm -rf $repoNew/.DS_Store
	rm -rf $repoNew/.gitignore
	sudo mkdir /var/www/html/$repoNew
	sudo rsync -avr $repoNew/ /var/www/html/$repoNew/
	cd /var/www/html/$repoNew/
	sed -i "s/2.1.0/$rubyVersion/g" Gemfile
	bundle install


	#Install and create database configuration file
	gem install mysql2
	mysql -u root -p$mysqlRoot -e "create user jss_cv"
	mysql -u root -p$mysqlRoot -e "grant all on *.* to jss_cv"
	mysql -u root -p$mysqlRoot -e "flush privileges"

#Use a here file to create database.yml
cat <<< 'default: &default
  adapter: mysql2
  encoding: utf8
  pool: 5
  username: '$localDBUser'
  password: '$localDBPass'

reports_database_development:
  <<: *default
  database: '$user'
  username: '$rpnDBUser'
  password: '$rpnDBPass'
  host: '$rpnIP'
  port: '$DBPort'
dash_database_development:
  <<: *default
  database: '$user'
  username: '$dashDBUser'
  password: '$dashDBPass'
  host: '$dashIP'
  port: '$DBPort'
development:
  <<: *default
  database: crowd_vision_portal
  password:


reports_database_production:
  <<: *default
  database: '$user'
  username: '$rpnDBUser'
  password: '$rpnDBUser'
  host: '$rpnIP'
  port: '$DBPort'
dash_database_production:
  <<: *default
  database: '$user'
  username: '$dashDBUser'
  password: '$dashDBPass'
  host: '$dashIP'
  port: '$DBPort'
production:
  <<: *default
  database: crowd_vision_portal
  password:' > $dbFile


	#Uncomment the devise secret key
	secretKey=$(grep "config.secret_key" config/initializers/devise.rb | grep -v "#" | tr -s " " | cut -d " " -f4 | tr -d "'")
	sed -i "s/<%= ENV\[\"SECRET_KEY_BASE\"\] %>/$secretKey/" config/secrets.yml
	RAILS_ENV=production rake db:create
	RAILS_ENV=production rake db:schema:load
	RAILS_ENV=production rake db:seed
	RAILS_ENV=production rake db:setup
	RAILS_ENV=production bundle exec rake assets:precompile

	#Setup root user
	RAILS_ENV=production rails runner "Setting.create!({:account_allowed => 1})"
	RAILS_ENV=production rails runner "User.create!({:email => \"$rootEmail\", :password => \"$rootPw\", :password_confirmation => \"$rootPw\", :role => 4 })" -->
